#include <Encoders.h>

/*
 * Connect controller pins to encoder:
 */
const uint8_t pin_CS = 4;
const uint8_t pin_CLK = 5;
const uint8_t pin_D0 = 6;

EMS22A e(4, 5, 6);

void setup() {
  e.begin();

  Serial.begin(9600);
}

void loop() {
  Serial.println(e.mpos(), DEC);
  delay(500);
}

