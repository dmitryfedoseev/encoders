#include <Encoders.h>

/*
 * Connect controller pins to encoder:
 * Encoder : Controller
 * P1      : 8
 * P2      : 9
 * P3      : 10
 * P4      : 11
 * P5      : 4
 * P6      : 5
 * P7      : 5
 * P8      : 5
 */
AMS128 e;

void setup() {
  e.begin();

  Serial.begin(9600);
}

void loop() {
  Serial.println(e.mpos(), DEC);
  delay(500);
}

