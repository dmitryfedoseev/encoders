#include <Encoders.h>

class TrivialEncoder : public EncoderBase {
  public:
    TrivialEncoder() : EncoderBase(16) {};
    uint16_t physicalPos() {
      return 0;
    };
};

TrivialEncoder e;

void setup() {
  e.begin();

  Serial.begin(9600);
}

void loop() {
  Serial.println(e.mpos(), DEC);
  delay(500);
}

