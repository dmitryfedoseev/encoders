/*
 * EncoderBase - base library for any kind of angular sersors.
 * Created by Dmitry Fedoseev, Dec 20, 2017.
 * Released into the public domain.
 *
 * This library defines single class used to be base of almost any kind of angular sensors.
 * It handles only logical operations like rotations count and zero offset.
 * Physycal interfacing left out of scope.
 *
 * To use library create own class that inherit everything from EncoderBase,
 * but overloads EncoderBase::physicalPos() method; it must return single sample.
 * For example, you can get sample from absolute encoder using SSI bus transaction. 
 * Another example, you can count pulses from relative encoder into counter variable 
 * using interrupt routines and then return value of this counter variable as physical
 * sample.
 */

#ifndef EncoderBase_h
#define EncoderBase_h

#include <Arduino.h>


/*
 * Abstract encoder.
 * Handles logical offset and counting turns.
 */
class EncoderBase {
  private:
    uint16_t _zero;                     // offset of logical zero
    uint16_t _lastpos;                  // used to detect roolups and rolldowns
    uint16_t _scale;                    // discretes per turn; 1024 for 10-bit encoder
    int16_t _mpos;                      // multiturn counter

  public:
    EncoderBase(uint16_t scale);
    void begin();                       // Initializes IO; call it from setup().

    /*
     * You MUST overload next method.
     */
    virtual uint16_t physicalPos() = 0; // Returns single sample of raw mechanical position; pure abtract.

    uint16_t upos();                    // Returns logical position from 0 to _scale-1.
    int16_t pos();                      // Returns logical position from -scale/2 to scale/2-1.
    int16_t mpos();                     // Returns multiturn position -32768 -> +32767 composed 
                                        //  from full and partial turns; LSB are upos() and MSB are full turns.
    /*
     * You can overload following two methods to extend max/min values of mpos().
     */
    void rollUp();                      // Called on mpos rolled up, increment mpos by _scale.
    void rollDown();                    // Called on mpos rolled down, decrement mpos by _scale.

    void setMpos(int16_t mPos);         // Sets current position to multiturn value.
    void setZero();                     // Sets logical zero to current position.
    void setZero(uint16_t rawPos);      // Sets logical zero position.
    uint16_t getZero();                 // Returns logical zero position.
}; // EncoderBase


/*
 * EMS22A - Non-Contacting Absolute Encoder.
 * Datashhet https://www.bourns.com/pdfs/EMS22A.pdf
 *
 * Encoder : Conroller
 * DI(1)   : GND
 * CLK(2)  : <pin_clk>
 * GND(3)  : GND
 * DO(4)   : <pin_data>
 * VCC(5)  : VCC(+5V)
 * CS(6)   : <pin_cs>
 */
class EMS22A : public EncoderBase {
  private:
    uint8_t _pin_cs;                    // CS line (encoder pin 6)
    uint8_t _pin_clk;                   // CLK line (encoder pin 2)
    uint8_t _pin_data;                  // DO line (encoder pin 4)
    uint8_t _tclk;                      // CLK high/low level duration, usec
  public:
    EMS22A(uint8_t pin_cs, uint8_t pin_clk, uint8_t pin_data, uint8_t tclk=2);
    uint16_t physicalPos();
};


/*
 * AMS128 -  Absolute Contacting Encoder
 * Datasheet https://www.bourns.com/pdfs/ace.pdf
 * Encoder : Controller
 * P1      : 8
 * P2      : 9
 * P3      : 10
 * P4      : 11
 * P5      : 4
 * P6      : 5
 * P7      : 5
 * P8      : 5
 */
class AMS128 : public EncoderBase {
  private:
    PROGMEM static const uint8_t encoderMap_12345678[256];

  public:
    AMS128();
    uint16_t physicalPos();
};
#endif
